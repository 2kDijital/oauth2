<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;
  protected $fillable = [
      'name', 'email', 'password',
  ];
  protected $hidden = [
       'password', 'remember_token',
  ];

  public function address()
  {
      return $this->hasMany(Address::class);
  }
}
