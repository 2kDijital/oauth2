<?php

namespace App\Providers;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
      Protected $policies = [
         'App\Model' => 'App\Policies\Modelpolicy'
       ];
   Public function boot()
     {
     $this->registerPolicies();
     passport::routes();
}}
